import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {
  places: Array<any> = [];
    constructor() {
      this.places = [
        {
            imgSrc: 'assets/images/card-1.jpg',
            place: 'Cozy 5 Stars Apartment',
            description:
                // tslint:disable-next-line:max-line-length
                'The place is close to Gold Coast Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.',
            charge: '$899/night',
            location: 'Brisbane, QLD, Australia'
        },
        {
            imgSrc: 'assets/images/card-2.jpg',
            place: 'Office Studio',
            description:
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the night life in Sydney, NSW.',
            charge: '$1,119/night',
            location: 'Sydney, NSW, Australia'
        },
        {
            imgSrc: 'assets/images/card-3.jpg',
            place: 'Beautiful Castle',
            description:
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Melbourne City.',
            charge: '$459/night',
            location: 'Melbourne, Vic, Australia'
        }
    ];
    }

    ngOnInit() {}
}
